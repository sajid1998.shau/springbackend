const express = require('express');
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 8080;

// Middleware für das Parsen von Anfragen im JSON-Format
app.use(bodyParser.json());
app.use(cors());
// POST-Endpunkt für das Verarbeiten des Kontaktformulars
app.post('/send-email', (req, res) => {
  const { name, email, message } = req.body;

  // E-Mail-Konfiguration für Strato
  const transporter = nodemailer.createTransport({
    host: 'smtp.strato.de',
    port: 587,
    secure: false, // true für 465, false für andere Ports
    auth: {
      user: 'info@sas-ft.de', // Deine Strato-E-Mail-Adresse
      pass: 'Quer_summe.1'       // Dein Strato-E-Mail-Passwort
    }
  });

  // E-Mail-Optionen
  const mailOptions = {
    from: 'info@sas-ft.de',
    to: 's.shaukat@web.de', // Ziel-E-Mail-Adresse
    subject: 'Kontaktformular-Anfrage',
    text: 'Name: E-Mail:Nachricht:'
  };

  // E-Mail senden
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.status(500).send(error.toString());
    }
    res.status(200).send('E-Mail erfolgreich versendet: ' + info.response);
  });
});

// Server starten
app.listen(port, () => {
  console.log(`Server läuft auf http://localhost:${port}`);
});
